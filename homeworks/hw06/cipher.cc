#include "cipher.h"

#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}
string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here
    
    char back[27];
    for (int i = 0; i < 27; ++i) {
        char c = (char)('a' + i);
        if (i==26) c= ' ';
        char b = this->smile->cipherText[i];
        int idx = b-'a';
        if (b==' ') idx = 26;
        back[idx] = c;
    }
    
    for(unsigned int i = 0; i < enc.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(enc[i] == ' ') {
            pos = 26;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a';
        } else {
            pos = enc[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(back[pos]);
        } else {
            retStr += back[pos];
        }
    }

    cout << "Done" << endl;

    return retStr;
}




struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    // Fill in code here
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
    CaesarSmile->rot = 0;

}

CaesarCipher::CaesarCipher(string in, int rot)
{
    // Fill in code here
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = in;
    CaesarSmile->rot = rot;
}

string CaesarCipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    // Fill in code here

    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        pos= (pos+this->CaesarSmile->rot+27)%27;
        if(upper) {
            retStr += UPPER_CASE(CaesarSmile->cipherText[pos]);
        } else {
            retStr += CaesarSmile->cipherText[pos];
        }
    }


    cout << "Done" << endl;

    return retStr;

}

string CaesarCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here

    char back[27];
    for (int i = 0; i < 27; ++i) {
        char c = (char)('a' + i);
        if (i==26) c= ' ';
        char b = this->CaesarSmile->cipherText[i];
        int idx = b-'a';
        if (b==' ') idx = 26;
        back[idx] = c;
    }

    for(unsigned int i = 0; i < enc.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(enc[i] == ' ') {
            pos = 26;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a';
        } else {
            pos = enc[i] - 'A';
            upper = 1;
        }
        pos= (pos-this->CaesarSmile->rot+27)%27;
        if(upper) {
            retStr += UPPER_CASE(back[pos]);
        } else {
            retStr += back[pos];
        }
    }


    cout << "Done" << endl;

    return retStr;
}

CaesarCipher &CaesarCipher::operator++() {
    this->CaesarSmile->rot += 5;
    this->CaesarSmile->rot = this->CaesarSmile->rot%27;
    return *this;
}

CaesarCipher &CaesarCipher::operator--() {
    this->CaesarSmile->rot -= 5;
    this->CaesarSmile->rot = (this->CaesarSmile->rot+27)%27;
    return *this;
}


